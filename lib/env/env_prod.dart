import 'package:envied/envied.dart';

part 'env_prod.g.dart';

@Envied(path: '.env.prod', obfuscate: true)
abstract class ProdEnv {
  @EnviedField(varName: 'SECRET_ANDROID')
  static final String secretAndroid = _ProdEnv.secretAndroid;
  @EnviedField(varName: 'API_HOST')
  static final String apiHost = _ProdEnv.apiHost;
}
