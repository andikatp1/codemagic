import 'package:envied/envied.dart';

part 'env_dev.g.dart';

@Envied(path: '.env.dev', useConstantCase: true)
abstract class DevEnv {
    @EnviedField(varName: 'SECRET_ANDROID')
    static const String secretAndroid = _DevEnv.secretAndroid;
    @EnviedField(varName: 'API_HOST')
    static const String apiHost = _DevEnv.apiHost;
}